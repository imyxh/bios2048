.code16
.global _start


_start:
# TODO: use rep movsb to copy this memory into a place after itself to avoid bpb bullshit
# then jump ofc
	subw $0x10, %sp
play:
	xorb %cl, %cl
	clear_screen:
		callw crlf
		incb %cl
		cmpb $0x20, %cl
		jl clear_screen
	leaw board, %si
	callw place_new
	callw print_board
	callw getc
	xorw %cx, %cx
	cmpb $0x68, %al	# h
	je left
	cmpb $0x6A, %al	# j
	je down
	cmpb $0x6B, %al	# k
	je up
	cmpb $0x6C, %al	# l
	je right
	jmp play

# movements
left:
	callw merge
	incw %cx
	addw $8, %si
	cmpw $4, %cx
	jl left
	jmp play
down:
	pushw %si
	leaw -8(%bp), %di
	callw rotate_col
	xchgw %di, %si
	leaw -0x10(%bp), %di
	callw flip_row
	xchgw %di, %si
	callw merge
	leaw -8(%bp), %di
	callw flip_row
	xchgw %di, %si
	popw %di
	callw rotate_row
	xchgw %di, %si
	incw %cx
	addw $2, %si
	cmpw $4, %cx
	jl down
	jmp play
up:
	leaw -8(%bp), %di
	callw rotate_col
	xchgw %si, %di
	callw merge
	callw rotate_row
	xchgw %si, %di
	incw %cx
	addw $2, %si
	cmpw $4, %cx
	jl up
	jmp play
right:
	leaw -8(%bp), %di
	callw flip_row
	xchgw %si, %di
	callw merge
	callw flip_row
	xchgw %si, %di
	incw %cx
	addw $8, %si
	cmpw $4, %cx
	jl right
	jmp play


# copy row at %si into row at %di, flipped
flip_row:
	pushw %cx
	xorw %cx, %cx
	movw $3, %dx
	transpose0:
		movw (%esi, %ecx, 2), %ax
		movw %ax, (%edi, %edx, 2)
		incw %cx
		decw %dx
		cmpw $4, %cx
		jl transpose0
	popw %cx
	retw

# rotate column anticlockwise with upper cell at %si into row at %di
rotate_col:
	pushw %cx
	xorw %cx, %cx
	transpose1:
		movw (%esi, %ecx, 8), %ax
		movw %ax, (%edi, %ecx, 2)
		incw %cx
		cmpw $4, %cx
		jl transpose1
	popw %cx
	retw

# rotate row at %si clockwise into column with upper cell at %di
rotate_row:
	pushw %cx
	xorw %cx, %cx
	transpose2:
		movw (%esi, %ecx, 2), %ax
		movw %ax, (%edi, %ecx, 8)
		incw %cx
		cmpw $4, %cx
		jl transpose2
	popw %cx
	retw

getc:
	xorb %ah, %ah	# get character
	int $0x16	# BIOS keyboard
	retw

# merge (in place) left the row at %si
merge:
	pusha
	xorw %cx, %cx
	xorw %di, %di
	# displace zeroes with gravity
	gravity:
		xorw %dx, %dx
		swapper:
			cmpw $3, %dx
			je swapping_done
			leaw (%esi, %edx, 2), %ax
			# skip swapping if arr[i] is nonzero
			cmpw $0, (%eax)
			jne next_swap
			movw 2(%esi, %edx, 2), %bx
			movw %bx, (%eax)
			movw $0, 2(%esi, %edx, 2)
			next_swap:
				incw %dx
				jmp swapper
		swapping_done:
		# run gravity three times (for case 0001)
		incw %cx
		cmpw $3, %cx
		jl gravity

	# merge matches
	match_ab:
		leaw (%esi, %edi, 2), %ax
		leaw 2(%esi, %edi, 2), %bx
		movw (%bx), %dx
		cmpw (%eax), %dx
		je merge_ab
		incw %di
		cmpw $3, %di
		jl match_ab
		popa
		retw
	merge_ab:
		shlw %dx
		movw %dx, (%eax)
		movw $0, (%bx)
		incw %di
		movw %di, %cx
		jmp gravity

print_board:
	pushw %si
	movw $board, %si
	xorw %cx, %cx
	print_row:
		callw puti
		addw $2, %si
		callw puti
		addw $2, %si
		callw puti
		addw $2, %si
		callw puti
		addw $2, %si
		callw crlf
		callw crlf
		incw %cx
		cmpw $4, %cx
		jl print_row
	popw %si
	retw

# print uint16 at %si to console
puti:
	push %cx
	movw $10000, %cx
	movw (%si), %di
	place:
		movw %di, %ax
		xorw %dx, %dx
		divw %cx
		# subtract from %di for the next round
		movw %ax, %dx
		imulw %cx, %dx
		subw %dx, %di
		addb $0x30, %al
		# replace zero with space
		cmpb $0x30, %al
		movw $0x20, %dx
		cmovew %dx, %ax
		movb $0x0E, %ah	# display function
		int $0x10	# BIOS video
		xchgw %ax, %cx
		movw $10, %cx
		xorw %dx, %dx
		divw %cx
		xchgw %ax, %cx
		testw %cx, %cx
		jnz place
	pop %cx
	retw

crlf:
	movb $0x0E, %ah
	movb $0x0D, %al
	int $0x10
	movb $0x0E, %ah
	movb $0x0A, %al
	int $0x10
	retw

# place a new 2 or 4 in board at %si
place_new:
	xorw %bx, %bx
	try_place:
		incw %bx
		# read RTC
		movb $0x00, %ah
		int $0x1A
		# reduce to 0~15
		andw $0xF, %dx
		# give up trying after a while :)
		cmpw $0xFF, %bx
		jg give_up
		# skip if cell is unavailable
		cmpw $0, (%esi, %edx, 2)
		jne try_place
	xchgw %dx, %bx
	# read RTC to pick 2/4
	movb $0x00, %ah
	int $0x1A
	# reduce to 0~3
	movw $2, %ax
	movw $4, %cx
	andw $0x3, %dx
	# 0.25 chance we place a 4
	cmovzw %cx, %ax
	movw %ax, (%esi, %ebx, 2)
	give_up:
		retw


board:	.fill 4*4, 2, 0

.fill 510-(.-_start), 1, 0xFF
.word 0xAA55

